The main of the Memorizer / Generator:
1. help you learn english
2. based on your new vocabulary, create texts for you which help you understand something new (you decided the subject)
3. help you create content (your subject, or the Memorizer can offer to you) pr improve self based on your new vocabulary (you can choose a learning rate (amount vocabulary has to be used in your content))

## Creating a Virtual Environment and Installing Dependencies

To set up a virtual environment for your project and install dependencies, follow these steps:

1. **Install Python 3.11:**

   Make sure you have Python 3.11 installed on your system. You can download it from the official Python website.

2. **Create a Virtual Environment:**

   Open your terminal or command prompt and navigate to your project directory. Then, run the following command to create a virtual environment. Replace `<venv>` with your preferred virtual environment name:

   ```bash
   python3.11 -m venv <venv>
   ```
   
3. ** Activate the Virtual Environment:On Windows:**
On Windows: 
   ```bash 
   .\<venv>\Scripts\activate
   ```
On macOS and Linux: 
   ```bash
   source <venv>/bin/activate
   ```
Your command prompt should now show the virtual environment name, indicating that it's active.

4. ** Install Dependencies: **
Use pip to install the project's dependencies from the requirements.txt file. Replace <path_to_requirements.txt> with the actual path to your requirements.txt file:
   ```bash
   pip install -r <path_to_requirements.txt>
   ```
This will install all the necessary packages for your project.

5. ** Deactivate the Virtual Environment: **
This returns you to your system's global Python environment.
Now you have a virtual environment set up for your project with all the required dependencies installed. You can activate it whenever you work on your project to isolate its dependencies from the system-wide Python installation.


## Creating Documentation

1. ** Open 'Controls' directory **
On Linux: 
   ```bash 
   cd ./memorizer/controls
   ```

2. ** Run the next command **
On Linux: 
   ```bash 
   $ sphinx-quickstart
   ```

3. Follow the instructions of sphinx